# Versioning

## What does it serve?

* Consumers are aware of breaking changes, features and hotfixes
* It's almost mandatory to be able to include the version in the software
  itself.
  We want our users be able to gather this version to report issues.
  Even if we don't show it and include some sort of "report bug"
  functionality in the app itself, we want that report to include this
  information.

This version can be maintained "manually"* or automatically.

\* Maintenance is almost never fully manual, there's usually a step in the
release pipelines that automatically increases the version or something
along those lines.

### Manual maintenance

In this case, the version (string) is kept in the project repository.

#### Pros

* Completely reproducible builds from the source code
* No dependency from the build-system on external software
  This software could change - e.g.: git
* While this exists in the Java ecosystem, other programming languages might not have the same plugins available for their build system

#### Cons

* Either it is taken care of manually or there's a "separate" automation
  taking care of it
* Usually trying to automate this requires a set of credentials with "push
  to master" permissions
* Generates noise with somewhat meaningless commits
* "Deploy" on master actually means "there's still some chores to do"
* `git rebase`s might get trickier

### Automatic maintenance

[jgitver](https://jgitver.github.io/) (no knowledge regarding alternatives
outside of the Java ecosystem) computes the version "automatically".

### Pros

* No chore work to release
* Doesn't rely on external automation
* Doesn't require credentials to make the "Upgraded version" commit
  (although if we want to fully automate this we might need one to push a
  tag...)
* No code changes on this "chore" task, which actually avoids unnecessary
  difficulties like `git rebase` with possible version conflicts

### Cons

* Tagging is still necessary, and could be somewhat error prone
* Relies on external software - in this case, `git`
  The history won't be fully reproducible without it, but, to be honest,
  without it we wouldn't have the same "history" anyway
* If you tag an older commit, that commit won't produce the same build
  now...
* The "need for a separate commit" actually had some use cases aside from
  the "chore"
  It could be used, as an example, to consolidate release notes.

## SemVer
